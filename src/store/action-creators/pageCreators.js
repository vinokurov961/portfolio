import { PageActionTypes } from "../../types/page";

export const changePageNumber = (pageNumber) => {
  return (dispatch) => {
    dispatch({
      type: PageActionTypes.CHANGE_PAGE_NUMBER,
      payload: pageNumber,
    });
  };
};
