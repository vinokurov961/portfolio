import * as PageCreators from "./pageCreators";

export default {
  ...PageCreators,
};
