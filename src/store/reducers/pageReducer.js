import { PageActionTypes } from "../../types/page";

const initialState = {
  page_number: 1
};

export const pageReducer = (state = initialState, action) => {
  switch (action.type) {
    case PageActionTypes.CHANGE_PAGE_NUMBER:
      return { ...state, page_number: action.payload };
    default:
      return state;
  }
};
