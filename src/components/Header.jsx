import React, { useEffect, useState } from "react";
import styles from "./../assets/styles/header.module.scss";
import { useSelector, useDispatch } from "react-redux";
import { changePageNumber } from "../store/action-creators/pageCreators";

const Header = () => {
  const { page } = useSelector((state) => state);
  const dispatch = useDispatch();

  const [themePage, setThemePage] = useState("_black");
  const [mobileMenu, setSobileMenu] = useState(false);

  const changeTheme = () => {
    setThemePage(themePage === "_black" ? "_white" : "_black");
  };

  useEffect(() => {
    document.body.style.setProperty(
      "--pageColor",
      `var(--pageColor${themePage})`
    );
    document.body.style.setProperty(
      "--mainColor",
      `var(--mainColor${themePage})`
    );
    document.body.style.setProperty(
      "--anotherColor",
      `var(--anotherColor${themePage})`
    );
    document.body.style.setProperty(
      "--anotherColoOpacity",
      `var(--anotherColoOpacity${themePage})`
    );
    document.body.style.setProperty(
      "--rgb_anotherColoOpacity",
      `var(--rgb_anotherColoOpacity${themePage})`
    );
  }, [themePage]);

  return (
    <header className={styles.header}>
      <div className={styles.header__container}>
        <div className={styles.header__top}>
          <p className={styles.header__contacts}>
            <span
              onClick={(e) => {
                window.location.href = `tel:+ 7 (919) 380 64-25`;
                e.preventDefault();
              }}
            >
              + 7 (919) 380 64-25
            </span>
          </p>
          <p className={styles.header__contacts}>
            <span
              onClick={(e) => {
                window.location.href = `mailto:vinok2002@mail.ru`;
                e.preventDefault();
              }}
            >
              vinok2002@mail.ru
            </span>
          </p>
        </div>
        <div className={styles.header__body}>
          <a
            className={styles.header__mainLink}
            href="#"
            onClick={() => dispatch(changePageNumber(1))}
          >
            Главная
          </a>
          <nav className={styles.header__nav}>
            <div className={styles.header__navInner}>
              <a
                onClick={() => dispatch(changePageNumber(2))}
                className={
                  styles.header__navInner__link +
                  " " +
                  (page.page_number === 2
                    ? styles.header__navInner__link_active
                    : "")
                }
                href="#"
              >
                Обо мне
              </a>
              <a
                onClick={() => dispatch(changePageNumber(3))}
                className={
                  styles.header__navInner__link +
                  " " +
                  (page.page_number === 3
                    ? styles.header__navInner__link_active
                    : "")
                }
                href="#"
              >
                Мои работы
              </a>
              <a
                onClick={() => dispatch(changePageNumber(4))}
                className={
                  styles.header__navInner__link +
                  " " +
                  (page.page_number === 4
                    ? styles.header__navInner__link_active
                    : "")
                }
                href="#"
              >
                Резюме
              </a>
              <button
                className={styles.header__navInner__button}
                onClick={() => changeTheme()}
              >
                Сменить тему
              </button>
              <span className={styles.header__navInner__buttonMobile} onClick={() => setSobileMenu(true)}></span>
            </div>
          </nav>
        </div>
      </div>
      <div className={styles.header__mobileMenu + " " + (mobileMenu ? styles.header__mobileMenu__active : "")}>
        <div className={styles.header__mobileMenu__bg} onClick={() => setSobileMenu(false)}>
        </div>
        <div className={styles.header__mobileMenu__body}>
          <nav className={styles.header__mobileMenu__nav}>
            <div className={styles.header__mobileMenu__navInner}>
              <a
                onClick={() => dispatch(changePageNumber(2))}
                className={
                  styles.header__mobileMenu__navInner__link +
                  " " +
                  (page.page_number === 2
                    ? styles.header__mobileMenu__navInner__link_active
                    : "")
                }
                href="#"
              >
                Обо мне
              </a>
              <a
                onClick={() => dispatch(changePageNumber(3))}
                className={
                  styles.header__mobileMenu__navInner__link +
                  " " +
                  (page.page_number === 3
                    ? styles.header__mobileMenu__navInner__link_active
                    : "")
                }
                href="#"
              >
                Мои работы
              </a>
              <a
                onClick={() => dispatch(changePageNumber(4))}
                className={
                  styles.header__mobileMenu__navInner__link +
                  " " +
                  (page.page_number === 4
                    ? styles.header__mobileMenu__navInner__link_active
                    : "")
                }
                href="#"
              >
                Резюме
              </a>
            </div>
          </nav>
        </div>
      </div>
    </header>
  );
};

export default Header;
