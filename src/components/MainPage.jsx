import React, { useEffect, useRef, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import styles from "../assets/styles/mainPage.module.scss";
import useThrottle from "../hooks/useThrottle";
import PageCountSlider from "./PageCountSlider";
import meImg from "../assets/images/me.png";
import dockerImg from "../assets/images/docker_img.png";
import psqlImg from "../assets/images/psql_img.png";
import reactImg from "../assets/images/react_img.png";
import rubyImg from "../assets/images/ruby_img.png";
import webpackImg from "../assets/images/webpack_img.png";
import nodeJsImg from "../assets/images/node_js.png";
import slider_1 from "../assets/images/slider_1.jpg";
import slider_2 from "../assets/images/slider_2.jpg";
import slider_3 from "../assets/images/slider_3.jpg";
import slider_4 from "../assets/images/slider_4.jpg";
import slider_5 from "../assets/images/slider_5.jpg";
import slider_6 from "../assets/images/slider_6.jpg";
import slider_7 from "../assets/images/slider_7.jpg";
import slider_8 from "../assets/images/slider_8.jpg";
import slider_9 from "../assets/images/slider_9.jpg";
import slider_10 from "../assets/images/slider_10.jpg";
import slider_11 from "../assets/images/slider_11.jpg";
import slider_12 from "../assets/images/slider_12.jpg";
import { changePageNumber } from "../store/action-creators/pageCreators";

const MainPage = () => {
  const { page } = useSelector((state) => state);
  const dispatch = useDispatch()
  const [resumeBg, setrResumeBg] = useState(0);

  const mainBlockRef = useRef();
  const sliderBlockRef = useRef();
  const resumeBlockRef = useRef();

  const [sliderBlockWidth, setSliderBlockWidth] = useState({
    transform: "translate(0, 0)",
    count: 0,
  });

  const callback = () => {
    if (window.pageYOffset === 10) return;
    if (window.pageYOffset > 42 && page.page_number !== 4) {
      dispatch(changePageNumber(page.page_number + 1))
      window.scrollTo({ top: 40, behavior: "smooth" });
    }
    if (window.pageYOffset < 38 && page.page_number !== 1) {
      dispatch(changePageNumber(page.page_number - 1))
      window.scrollTo({ top: 40, behavior: "smooth" });
    }
  };

  const throttleScrollMove = useThrottle(callback, 100);
  const throttleMobileScrollMove = useThrottle(callback, 1000);

  const mobileAndTabletCheck = function () {
    if (window.screen.width <= 768) {
      return true
    }
    return false
  };

  useEffect(() => {
    if (window.pageYOffset !== 40) {
      if (mobileAndTabletCheck()) {
        window.addEventListener("scroll", throttleMobileScrollMove);
      } else {
        window.addEventListener("scroll", throttleScrollMove);
      }
    }
    if (mobileAndTabletCheck()) {
      return () => window.removeEventListener("scroll", throttleMobileScrollMove);
    } else {
      return () => window.removeEventListener("scroll", throttleScrollMove);
    }
  }, [window.pageYOffset && page.page_number]);

  useEffect(() => {
    const itemsSlider = sliderBlockRef.current.querySelectorAll("li");
    const lengthSlider = itemsSlider.length;
    const weightItemSlider = itemsSlider[0].offsetWidth;

    let rotationInterval = setInterval(() => {
      if (sliderBlockWidth.count === lengthSlider - 1) {
        setSliderBlockWidth({
          ...sliderBlockWidth,
          transform: `translate(-${
            weightItemSlider * sliderBlockWidth.count
          }px, 0px)`,
          count: 0,
        });
      } else {
        setSliderBlockWidth({
          ...sliderBlockWidth,
          transform: `translate(-${
            weightItemSlider * sliderBlockWidth.count
          }px, 0px)`,
          count: sliderBlockWidth.count + 1,
        });
      }
    }, 3000);

    return () => {
      clearInterval(rotationInterval);
    };
  }, [sliderBlockWidth.transform]);

  useEffect(() => {
    let width = resumeBlockRef.current.offsetWidth;
    let height = resumeBlockRef.current.offsetHeight;
    setrResumeBg(Math.round((width * height) / 3050));
  }, []);

  const getTransForm = () => {
    if (page.page_number !== 1) {
      if (mobileAndTabletCheck()) {
        return {
          transform: `translate(0px, calc(${page.page_number - 1} * -100vh ))`,
        };
      } else {
        return {
          transform: `translate(0px, calc(${page.page_number - 1} * -100vh - (80px * ${
            page.page_number - 1
          } + 40px)))`,
        };
      }
    }
  };

  const getResumeBg = () => {
    let layout = [];
    for (let k = 0; k < resumeBg; k++) {
      layout.push(
        <div key={k}>
          <span></span>
        </div>
      );
    }
    return layout;
  };

  return (
    <>
      <main
        ref={mainBlockRef}
        className={"page " + styles.mainPage}
        style={getTransForm()}
      >
        <section
          className={styles.mainPage__slide + " " + styles.mainPage__first}
        >
          <div className={styles.mainPage__container}>
            <div className={styles.mainPage__first__body}>
              <h1 className="title">Мое портфолио</h1>
              <br />
              <p className="subtitle">
                Здравствуйте!
                <br />
                Меня зовут Винокуров&nbsp;Евгений Алексеевич, я
                <span className="anotherColor"> фронтенд</span> разработчик и
                хочу продемонстрировать вам свои навыки в программировании и
                немного рассказать о себе.
              </p>
              <hr />
              <p className="subtitle">
                <span className="anotherColor">
                  Мои контактные данные находятся в шапке страницы.
                </span>
              </p>
            </div>
          </div>
        </section>
        <section
          className={styles.mainPage__slide + " " + styles.mainPage__about}
        >
          <div className={styles.mainPage__container}>
            <div className={styles.mainPage__about__body}>
              <p className={styles.mainPage__about__in}>Обо мне</p>
              <h2 className={styles.mainPage__about__title + " title"}>
                Чем я занимаюсь ?
              </h2>
              <hr />
              <p className={styles.mainPage__about__text}>
                Я работаю программистом менее года, но учу его более 4 лет. В
                компании, которой работаю, я занимаю должность{" "}
                <span className="anotherColor">
                  full&nbsp;stack&nbsp;разработчика
                </span>
                .
              </p>
              <br />
              <p className={styles.mainPage__about__text}>
                В мои обязанности как программиста входит:
              </p>
              <ul className={styles.mainPage__about__list}>
                <li>
                  <p className={styles.mainPage__about__text}>
                    Написание плана выполнения задач
                  </p>
                </li>
                <li>
                  <p className={styles.mainPage__about__text}>
                    Разработка и написание кода
                  </p>
                </li>
                <li>
                  <p className={styles.mainPage__about__text}>
                    Написание тестов на свой код
                  </p>
                </li>
                <li>
                  <p className={styles.mainPage__about__text}>
                    Исправление багов
                  </p>
                </li>
                <li>
                  <p className={styles.mainPage__about__text}>
                    Улучшение кода по замечаниям из code review
                  </p>
                </li>
              </ul>
            </div>
          </div>
          <div className={styles.mainPage__about__images}>
            <div className={styles.mainPage__about__images__block}>
              <div
                style={{ top: "0", left: "50%" }}
                className={styles.mainPage__about__images__item}
              >
                <img src={meImg} />
              </div>
              <div
                style={{ top: "15%", left: "0" }}
                className={styles.mainPage__about__images__item}
              >
                <img src={dockerImg} />
              </div>
              <div
                style={{ top: "75%", left: "0" }}
                className={styles.mainPage__about__images__item}
              >
                <img src={psqlImg} />
              </div>
              <div
                style={{ top: "100%", left: "50%" }}
                className={styles.mainPage__about__images__item}
              >
                <img src={reactImg} />
              </div>
              <div
                style={{ top: "75%", left: "100%" }}
                className={styles.mainPage__about__images__item}
              >
                <img src={rubyImg} />
              </div>
              <div
                style={{ top: "15%", left: "100%" }}
                className={styles.mainPage__about__images__item}
              >
                <img src={webpackImg} />
              </div>
              <div
                style={{ top: "50%", left: "50%" }}
                className={styles.mainPage__about__images__item}
              >
                <img src={nodeJsImg} />
              </div>
            </div>
          </div>
        </section>
        <section
          className={styles.mainPage__slide + " " + styles.mainPage__works}
        >
          <div className={styles.mainPage__container}>
            <div className={styles.mainPage__works__body}>
              <div className={styles.mainPage__works__top}>
                <p className={styles.mainPage__works__in}>Мои работы</p>
                <h2 className={styles.mainPage__works__title + " title"}>
                  Мои навыки
                </h2>
                <hr />
                <p className={styles.mainPage__works__text}>
                  Все возможные работы, которые я делаю не во время работы,
                  располагаются на gitlab.
                </p>
                <br />
                <p className={styles.mainPage__works__text}>
                  Ссылка на мой{" "}
                  <a
                    href="https://gitlab.com/vinokurov961"
                    target="_blank"
                    className="anotherColor" rel="noreferrer"
                  >
                    gitlab
                  </a>
                </p>
              </div>
              <div className={styles.mainPage__works__bottom}>
                <div className={styles.mainPage__works__slider}>
                  <ul
                    style={{ transform: sliderBlockWidth.transform }}
                    ref={sliderBlockRef}
                    className={styles.mainPage__works__sliderList}
                  >
                    <li className={styles.mainPage__works__sliderItem}>
                      <img src={slider_1} />
                    </li>
                    <li className={styles.mainPage__works__sliderItem}>
                      <img src={slider_2} />
                    </li>
                    <li className={styles.mainPage__works__sliderItem}>
                      <img src={slider_3} />
                    </li>
                    <li className={styles.mainPage__works__sliderItem}>
                      <img src={slider_4} />
                    </li>
                    <li className={styles.mainPage__works__sliderItem}>
                      <img src={slider_5} />
                    </li>
                    <li className={styles.mainPage__works__sliderItem}>
                      <img src={slider_6} />
                    </li>
                    <li className={styles.mainPage__works__sliderItem}>
                      <img src={slider_7} />
                    </li>
                    <li className={styles.mainPage__works__sliderItem}>
                      <img src={slider_8} />
                    </li>
                    <li className={styles.mainPage__works__sliderItem}>
                      <img src={slider_9} />
                    </li>
                    <li className={styles.mainPage__works__sliderItem}>
                      <img src={slider_10} />
                    </li>
                    <li className={styles.mainPage__works__sliderItem}>
                      <img src={slider_11} />
                    </li>
                    <li className={styles.mainPage__works__sliderItem}>
                      <img src={slider_12} />
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section
          className={styles.mainPage__slide + " " + styles.mainPage__resume}
        >
          <div className={styles.mainPage__resume__container}>
            <div className={styles.mainPage__resume__body}>
              <p className={styles.mainPage__resume__in}>Резюме</p>
              <h2 className={styles.mainPage__resume__title + " title"}>
                Обязательно к ознакомлению
              </h2>
              <hr />
              <p className={styles.mainPage__resume__text}>
                Мое резюме вы можете просмотреть на сайте{" "}
                <a
                  href="https://gitlab.com/vinokurov961"
                  target="_blank"
                  className="anotherColor" rel="noreferrer"
                >
                  hh.ru
                </a>
                , в нем более подробно описано, где я работаю(ал) и на каком
                стеке я пишу.
              </p>
              <br />
              <p className={styles.mainPage__resume__text}>
                Спасибо за проявленый интерес к моей персоне :)
              </p>
            </div>
            <div ref={resumeBlockRef} className={styles.mainPage__resume__bg}>
              {getResumeBg()}
            </div>
          </div>
        </section>
      </main>
      <PageCountSlider countPages={4} activePage={page.page_number} />
    </>
  );
};

export default MainPage;
