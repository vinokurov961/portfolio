import React, { useEffect, useRef } from "react";
import styles from "../assets/styles/pageCountSlider.module.scss";

const PageCountSlider = ({ countPages, activePage }) => {
  let pageCountRef = useRef();

  const getOpacity = (idElem) => {
    if (idElem + 1 === activePage || idElem - 1 === activePage)
      return { opacity: "0.75" };

    if (idElem + 2 === activePage || idElem - 2 === activePage)
      return { opacity: "0.5" };

    if (idElem > activePage || idElem < activePage)
      return { opacity: "0.25" };

    return { opacity: "1", transform: "translateX(0%)" };
  };

  const getCounts = () => {
    let layoutCounts = [];
    for (let k = 1; k < countPages + 1; k++) {
      layoutCounts.push(
        <div
          key={k}
          className={
            styles.pageCount__numbersItem +
            " " +
            (activePage === k ? styles.pageCount__numbersItem__active : "")
          }
          style={getOpacity(k)}
        >
          {`0${k}`}
        </div>
      );
    }
    return layoutCounts;
  };

  useEffect(() => {
    let heightPageCount = pageCountRef.current.clientHeight;
    pageCountRef.current.style["transform"] = `translateY(-${(heightPageCount / countPages) * activePage
      }px)`;
  }, [activePage]);

  return (
    <div className={styles.pageCount}>
      <div ref={pageCountRef} className={styles.pageCount__numbers}>
        {getCounts()}
      </div>
    </div>
  );
};

export default PageCountSlider;
