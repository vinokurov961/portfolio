import Helmet from 'react-helmet';
import Header from './components/Header';
import MainPage from './components/MainPage';
const App = () => {
  return (
    <>
      {/* <!-- Yandex.Metrika counter --> */}
      {/* <!-- /Yandex.Metrika counter --> */}
      <Helmet script={[{
        "type": "text/javascript",
        "innerHTML": `(function(m,e,t,r,i,k,a){m[i] = m[i] || function () { (m[i].a = m[i].a || []).push(arguments) };
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(89459680, "init", {
          clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });`
      }]}>
      <noscript>{`<div><img src="https://mc.yandex.ru/watch/89459680" style="position:absolute; left:-9999px;" alt="" /></div>`}</noscript>
      </Helmet>

      <Header />
      <MainPage />
    </> 
  );
}

export default App;
