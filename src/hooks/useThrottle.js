import { useRef } from "react";

const useThrottle = (callback, delay) => {
  const isThrottled = useRef(null);

  const throrrledCallback = () => {
    if (isThrottled.current) return;

    callback();
    isThrottled.current = true;
    setTimeout(() => (isThrottled.current = false), delay);
  }

  return throrrledCallback;
};

export default useThrottle;